<?php

include('connection.php');

if (isset($_POST['submit_convert'])) {

    require_once 'pdf.php';

    exit();
}
if ($_GET['code']) {
    $code = $_GET['code'];
    $query = "SELECT * FROM content WHERE Code = '$code'";
    $result = mysqli_query($connection, $query);

    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $content = $row['singleContent'];
        }
    }
} else {
    echo "kein Code mitgegeben";
    header('Location: index.php');
}


if (isset($_POST['submit'])) {
    if (isset($_POST['editor']) && !empty($_POST['editor'])) {

        $content = $_POST['editor'];
        echo "Variable Content<br>";

    } else {
        echo "keine Variable Content<br>";

        $empty_error = '<b class="text-danger text-center">
                      Bitte etwas in die textarea eintragen<b>';
    }

    if (isset($content) && !empty($content)) {

        $insert_q = "UPDATE content SET singleContent = '$content' WHERE code='$code'";

        echo $insert_q . "<br>";
        if (mysqli_query($connection, $insert_q)) {
          header("Location: optionen-cover.html");

        } else {
            echo mysqli_error($connection);
            $submit_error = "nicht möglich einzugeben<br>";
            echo "Daten nicht eingefügt";
        }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>ckeditor data save</title>
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="font-awesome/css/fontawesome.min.css">
    <script src="jquery-3.3.1.min.js"></script>
    <script src="ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="dropzone.js"></script>
</head>
<body>

<?php if (isset($submit_error)) echo $submit_error; ?>
<?php if (isset($empty_error)) echo $empty_error; ?>

<form class="" action="" method="post" enctype="multipart/form-data">
    <textarea class="ckeditor" name="editor"><?php if (isset($content)) echo $content; ?></textarea>

    <br>
    <button type="submit" name="submit">speichern</button>
    <button type="submit" name="submit_convert">Save and Get PDF</button>
</form>
</body>
</html>

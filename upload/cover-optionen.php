<!DOCTYPE html>
<html lang="de" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="styles/styles.css">
  </head>
  <body>
    <div class="bigBox">
      <p>Erstelle Deine Buchrolle in 4 einfachen Schritten</p>
      <div class="boxes">
      <div class="box" style="background: white">
      <p>1. Druckdateien</p>
      </div>
      <div class="box" style="background: white">
      <p><b>2. Cover</b></p>
      </div>
      <div class="box" style="background: white">
      <p>3. Menge</p>
      </div>
      <div class="box" style="background: white">
      <p>4. Überprüfen und kaufen</p>
      </div>
      </div>
    </div>

    <div class="bigBox">
      <p>Wähle nun aus, wie Du die Buchrolle gebunden haben möchtest</p>
      <div class="boxes">
        <div class="box">
          <img src="images/Buchrolle.jpg" class="imageDiv" alt="Buchrolle">
          <p>Gebunden</p>
          <ul>
            <li>Einband in Buchleinen</li>
            <li>So wird Dein Buch zur Buchrolle</li>
          </ul>
          <br>
          <br>
          <p>Preis ab XX,- €</p>
          <a class="link" href="uploadcover.php">Jetzt erstellen ></a>
        </div>
        <div class="box"> <!--needs to be only in styles.css-->
          <img src="images/Buchrolle.jpg" class="imageDiv" alt="Buchrolle">
          <p>Ungebunden</p>
          <ul>
            <li>Ideal zum Aufhängen</li>
            <li>Achtung: Der Druck wird komplett ohne Einband geliefert</li>
            <li>115g/m2 Papier</li>
            <li>Einband in Buchleinen</li>
          </ul>

          <p>Preis 0,- €</p>
          <a class="link" href="menge.php">Abschließen ></a>
        </div>
        <div class="box" style="background: white">

        </div>
        <div class="box" style="background: white">

        </div>
      </div>
    </div>

  </body>
</html>

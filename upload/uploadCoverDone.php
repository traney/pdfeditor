<?php
  if (isset($_POST['submit'])) {
    if (!file_exists($_FILES['aussencover']['tmp_name']) || !is_uploaded_file($_FILES['aussencover']['tmp_name']))
    {
      header('Location: uploadcover.php');
    }

    if (!file_exists($_FILES['innencover']['tmp_name']) || !is_uploaded_file($_FILES['innencover']['tmp_name']))
    {
      header('Location: uploadcover.php');
    }
    session_start();

    //Außencover
    $pdfFile = $_FILES['aussencover'];
    //print_r($pdfFile);

    //split array $_FILES
    $pdfName =$_FILES['aussencover']['name'];
    $pdfTmpName =$_FILES['aussencover']['tmp_name'];
    $pdfSize =$_FILES['aussencover']['size'];
    $pdfError =$_FILES['aussencover']['error'];
    $pdfType =$_FILES['aussencover']['type'];

    //get file type
    $fileExt = explode('.',$pdfName);
    $fileActualExt = strtolower(end($fileExt));

    $allowed = array('pdf', 'tif');

    if (in_array($fileActualExt, $allowed)) {
      if ($pdfError === 0) {
        if ($pdfSize < 5000000 ) {
          //number on servertime
          $pdfNameNew = uniqid('', true).".".$fileActualExt;
          $pdfDestination = 'uploads/'.$pdfNameNew;
          move_uploaded_file($pdfTmpName, $pdfDestination);
          //ggf header to php
        }else{
          echo"your file is too big";

          header('Location: uploadcover.php');
        }
      }else{
        echo"There was an error during the upload.";
        header('Location: uploadcover.php');

      }
    }else{
      echo "Only PDF or tiff is possible";
      header('Location: uploadcover.php');

    }
    //set Session
    $_SESSION["outerCover"] = $pdfDestination;


    //Innencover
    $pdfFile = $_FILES['innencover'];
    //print_r($pdfFile);

    //split array $_FILES
    $pdfName =$_FILES['innencover']['name'];
    $pdfTmpName =$_FILES['innencover']['tmp_name'];
    $pdfSize =$_FILES['innencover']['size'];
    $pdfError =$_FILES['innencover']['error'];
    $pdfType =$_FILES['innencover']['type'];

    //get file type
    $fileExt = explode('.',$pdfName);
    $fileActualExt = strtolower(end($fileExt));

    $allowed = array('pdf', 'tif');

    if (in_array($fileActualExt, $allowed)) {
      if ($pdfError === 0) {
        if ($pdfSize < 5000000 ) {
          //number on servertime
          $pdfNameNew = uniqid('', true).".".$fileActualExt;
          $pdfDestination = 'uploads/'.$pdfNameNew;
          move_uploaded_file($pdfTmpName, $pdfDestination);
          //ggf header to php
        }else{
          echo"your file is too big";

          header('Location: uploadcover.php');
        }
      }else{
        echo"There was an error during the upload.";
        header('Location: uploadcover.php');

      }
    }else{
      echo "Only PDF or tiff is possible";
      header('Location: uploadcover.php');

    }

    //set session
    $_SESSION["innerCover"] = $pdfDestination;

  }
   ?>


   <!DOCTYPE html>
   <html lang="de" dir="ltr">
     <head>
       <meta charset="utf-8">
       <link rel="stylesheet" href="styles/styles.css">
       <title>Upload your Preedited PDF</title>
       <title></title>
     </head>
     <body>


       <div class="bigBox">
      <p>Erstelle Deine Buchrolle in 4 einfachen Schritten</p>
      <div class="boxes">
      <div class="box" style="background: white">
      <p><b>1. Druckdateien</b></p>
      </div>
      <div class="box" style="background: white">
      <p>2. Cover</p>
      </div>
      <div class="box" style="background: white">
      <p>3. Menge</p>
      </div>
      <div class="box" style="background: white">
      <p>4. Überprüfen und kaufen</p>
      </div>
      </div>
      <iframe id="outerCover" src="1.pdf" width="49%" height="250">Ihr könnt das Bild nochmals downloaden und überprüfen</iframe>
      <iframe id="innerCover" src="1.pdf" width="49%" height="250">Ihr könnt das Bild nochmals downloaden und überprüfen</iframe>

      <br>
      <a href="menge.php">Weiter zur Menge</a>
    </div>

    <script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>
    <script type="text/javascript">

    //outerCover
      var pdfDestination = "<?php echo $_SESSION["outerCover"] ?>";

      console.log(pdfDestination);
       document.getElementById("outerCover").src = pdfDestination;

      // Loaded via <script> tag, create shortcut to access PDF.js exports.
      var pdfjsLib = window['pdfjs-dist/build/pdf'];

      // The workerSrc property shall be specified.
      pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

      // Using DocumentInitParameters object to load binary data.
      pdfjsLib.getDocument(pdfDestination).then(function(pdf) {
        console.log('PDF loaded');
      var numPages = pdf.numPages;
      console.log(numPages);

      // Fetch the first page

      var pageNumber = 1;
      var scale = 1;
      var width = -1;
      var height = -1;
      while (pageNumber <= numPages){
      pdf.getPage(pageNumber).then(function(page) {
        console.log('Page loaded');

        var viewport = page.getViewport(scale);
        if (height == -1) {
          height = viewport.height;
          if (!(500 < height < 600)){
            console.log( " height " + height );

            window.location.href = "upload.html";
          }
        }else if (height != viewport.height){
          console.log( "Final Width: " + width );

          window.location.href = "upload.html";
        }

        if (width == -1) {
          width = viewport.width;
        }else{
          width = width + viewport.width;
        }

        //Here's the width and height
         console.log( "Width: " + viewport.width + ", Height: " + viewport.height );
      });
      pageNumber++;
    }
    //Here's the width and height
     console.log( "Final Width: " + width + ", height " + height );
    // if ((width*5)<height) {
    //   window.location.href = "upload.html";
    // }

      });

      //innerCover
        var pdfDestination = "<?php echo $_SESSION["innerCover"] ?>";

        console.log(pdfDestination);
         document.getElementById("innerCover").src = pdfDestination;

        // Loaded via <script> tag, create shortcut to access PDF.js exports.
        var pdfjsLib = window['pdfjs-dist/build/pdf'];

        // The workerSrc property shall be specified.
        pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

        // Using DocumentInitParameters object to load binary data.
        pdfjsLib.getDocument(pdfDestination).then(function(pdf) {
          console.log('PDF loaded');
        var numPages = pdf.numPages;
        console.log(numPages);

        // Fetch the first page

        var pageNumber = 1;
        var scale = 1;
        var width = -1;
        var height = -1;
        while (pageNumber <= numPages){
        pdf.getPage(pageNumber).then(function(page) {
          console.log('Page loaded');

          var viewport = page.getViewport(scale);
          if (height == -1) {
            height = viewport.height;
            if (!(500 < height < 600)){
              console.log( " height " + height );

              window.location.href = "upload.html";
            }
          }else if (height != viewport.height){
            console.log( "Final Width: " + width );

            window.location.href = "upload.html";
          }

          if (width == -1) {
            width = viewport.width;
          }else{
            width = width + viewport.width;
          }

          //Here's the width and height
           console.log( "Width: " + viewport.width + ", Height: " + viewport.height );
        });
        pageNumber++;
      }
      //Here's the width and height
       console.log( "Final Width: " + width + ", height " + height );
      // if ((width*5)<height) {
      //   window.location.href = "upload.html";
      // }

        });



    </script>

     </body>
   </html>

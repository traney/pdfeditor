<!DOCTYPE html>
<html lang="de" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="styles/styles.css">
    <title>Upload your Preedited PDF</title>
  </head>
  <body>
    <div class="bigBox">
      <p>Erstelle Deine Buchrolle in 4 einfachen Schritten</p>
      <div class="boxes">
      <div class="box" style="background: white">
      <p><b>1. Druckdateien</b></p>
      </div>
      <div class="box" style="background: white">
      <p>2. Cover</p>
      </div>
      <div class="box" style="background: white">
      <p>3. Menge</p>
      </div>
      <div class="box" style="background: white">
      <p>4. Überprüfen und kaufen</p>
      </div>
      </div>
    </div>



    <div class="faqContainer">
      <h2>Jetzt kannst Du Deine Cover-Dateien hochladen:</h2>
      <p>Du kannst nur .pdf oder .tif Dateien hochladen</p>
      <p>Die Datei darf maximal 5MB groß sein</p>
      <p>Die Höhe jedes Covers muss 22cm betragen, die Breite 17cm.</p>
      <p>Noch keine Datei erstellt?<a href="getModelCover.html"> Dann gehts hier lang</a></p>

      <form action="uploadcoverDone.php" method="post" enctype="multipart/form-data">
        <p>Außencover: <input class="faq" type="file" name="aussencover" value=""></p>
        <p>Innencover: <input class="faq" type="file" name="innencover" value=""></p>
        <button class="faq" type="submit" name="submit">Beide Cover hochladen</button>
      </form>
      <br>
      <a  class="faq" href="#">FAQ ></a>

    </div>

  </body>
</html>
